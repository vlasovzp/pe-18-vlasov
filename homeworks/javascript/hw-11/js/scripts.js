const BTNS_EL = [...document.querySelectorAll('.btn')];
document.body.addEventListener('keyup', changeBtnColor);

function changeBtnColor(event) {
    const keyValue = event.key.toUpperCase();
    const btn = BTNS_EL.find(element => element.innerText.toUpperCase() === keyValue);

    if (!btn) return;
    else {
        const btnBlue = document.querySelector('.btn-blue');
        if (btnBlue) {
            btnBlue.classList.remove('btn-blue');
        }
        btn.classList.add('btn-blue');
    }
}
