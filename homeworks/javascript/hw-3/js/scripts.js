/**
 * desc Get values from user
 */
function getValues() {
    let num1;
    let num2;

    do {
        num1 = Number(prompt('Please, enter first number.'));
    } while (isNaN(num1) || num1 % 1 > 0);
    do {
        num2 = Number(prompt('Please, enter second number.'));
    } while (isNaN(num2) || num2 % 1 > 0);

    let mark = prompt('Please, enter math operation.');
    console.log(count(num1, num2, mark));
}

/**
 * @desc Create calculate function
 */

function count(num1, num2, mark) {
    switch (mark) {
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "*":
            return num1 * num2;
        case "/":
            if (num2 !== 0) {
                return num1 / num2;
            } else {
                return "на ноль не делится";
            }
    }
}
getValues();
