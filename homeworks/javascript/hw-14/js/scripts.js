$(document).ready(function () {

    /**
     * activeMenu
     */

    $('a[href^="#"]').click(function (event) {
        event.preventDefault();
        const elementClick = $(event.target).attr('href');
        const destination = $(elementClick).offset().top;

        $('html').animate({scrollTop: destination}, 1500);

        return false;

    });

    /**
     * sectionToggle
     */

    $('.btn-toggle').click(function () {
        $('.main-section-1').slideToggle(1000);
        return false;
    });


    /**
     * btnTop
    */
    const btnToTop = $('.scroll-top');
    const checkScrollingViewPort = (event) => {
        const scrolling = window.scrollY;
        const windowHeight = window.innerHeight;
        if (scrolling > windowHeight) {
            btnToTop.fadeIn(100);
        } else {
            btnToTop.fadeOut(100);
        }
    }
    checkScrollingViewPort();

    $(window).scroll(checkScrollingViewPort);

    btnToTop.on('click', () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    });


})