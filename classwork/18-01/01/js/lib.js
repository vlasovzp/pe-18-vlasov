function checkIfEmpty(val) {
    return val === '' || val === null;
}
