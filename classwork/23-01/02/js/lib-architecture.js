/**
 * @desc Функция которая считает высоту
 * @param fundament {number}
 * @param tsokol {number}
 * @param krusha {number}
 * @returns {number}
 */

function calcHeight(fundament = 10, tsokol = 5, krusha = 19) {
    return fundament + tsokol + krusha;
}

/**
 * @desc Площадь дания
 * @param a {number}
 * @param b {number}
 * @returns {number}
 */

function calcSquare(a, b) {
    return (a * b);
}