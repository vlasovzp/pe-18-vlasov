import React, {Component} from 'react';
import Button from "../Button/Button";
import './Modal.scss'

class Modal extends Component {
    render() {

        const { header, closeButton, text, actions, closeModal } = this.props;

        return (
            <div className='modal-pass' onClick={closeModal}>
                <div className="modal">
                    <div className="modal-header">
                        <p>{header}</p>
                        {closeButton && (
                            <Button
                                className='btn'
                                text='X'
                                onClick={closeModal}
                                backgroundColor="transparent"
                                style={{ fontSize: '25px' }}
                            />
                        )}
                    </div>
                    <div className="modal-body">
                        <p>{text}</p>
                        <div className="actions-wrapper">
                            {actions}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;