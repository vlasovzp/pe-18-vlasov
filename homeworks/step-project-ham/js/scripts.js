/**
 * active TAB
 */

const tabList = $('.main-tabs');
tabList.on('click', '.main-tabs-list', function (event) {
    $('.main-tabs-list.active').removeClass('active');
    $(event.target).addClass('active');
    $('.main-tabs-content-item.active').removeClass('active');
    const clickedAttrValue = $(event.target).attr('data-type');
    $(`.main-tabs-content-item[data-type='${clickedAttrValue}']`).addClass('active');
});

/**
 * simulated image upload
 */

const $galleryFilter = $('.amazing-work-filter');
const $gallery = $('.amazing-work');
const $loadBtn = $('.amazing-work-section .btn');
const $loader = $('.amazing-work-section .loader').hide();

const images = {
    'graphic-design': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    'landing-page': [1, 2, 3, 4, 5, 6, 7],
    'web-design': [1, 2, 3, 4, 5, 6, 7],
    wordpress: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] //TODO refact, maybe
};

const imagesArray = [...Object.entries(images)];

$loadBtn.click(buttonClicked);
$galleryFilter.click('.amazing-work-filter-item', filterClicked);
loadImages();

function randomIndex(length) {
    return Math.ceil(Math.random() * length) - 1;
}

function buttonClicked() {
    $loadBtn.hide(1);
    $loader.show(1);
    setTimeout(loadImages, 2000);
}

function getImageAttr(array) {
    const categoryIndex = randomIndex(array.length);
    const category = array[categoryIndex];
    const imageIndex = category[1].splice(randomIndex(category[1].length), 1);

    if (category[1].length === 0) {
        array.splice(categoryIndex, 1);
    }

    return {src: `images/${category[0]}/${category[0]}${imageIndex}.jpg`, category: category[0]};
}

function createGalleryItem() {
    const imageAttr = getImageAttr(imagesArray);
    const $newGalleryItem = $('<div class="amazing-work-item">').attr({'data-type': imageAttr.category});
    const $newImage = $(`<img alt="image" src=${imageAttr.src}>`);
    const $hoverDiv = createHoverDiv(imageAttr);
    $newGalleryItem.append($hoverDiv);
    $newGalleryItem.append($newImage);
    $gallery.append($newGalleryItem);
}

function loadImages() {
    for (let i = 0; i < 12; i++) {
        createGalleryItem();
    }

    if (!imagesArray.length) {
        $loadBtn.remove();
    }

    $galleryFilter.find('[data-type="all"]').trigger('click');
    $($loadBtn).show(1);
    $loader.hide(1);
}

function filterImages(event) {
    const type = $(event.target).data('type');
    const loadedImages = $('.amazing-work-item');

    if (type === 'all') {
        $(loadedImages).show('fast');
    } else {
        $(`.amazing-work-item[data-type='${type}']`).show('fast');
        $(`.amazing-work-item[data-type!='${type}']`).hide('fast');
    }
}

function filterClicked(event) {
    if ($(event.target).hasClass('active')) return;

    $('.amazing-work-filter').find('.active').removeClass('active');

    $(event.target).addClass('active');
    filterImages(event);
}

function createHoverDiv(imageAttr) {

    const $hoverDiv = $('<div class="amazing-work-hover-div">');
    const $linkIcon = $('<i class="fas fa-link">');
    const $searchIcon = $('<i class="fas fa-search">');
    const $heading = $('<h2 class="amazing-work-hover-div-title"> Creative Design </h2>');
    const $subheading = $(`<p class="amazing-work-hover-div-text">${imageAttr.category}</p>`);
    $hoverDiv.append($searchIcon);
    $hoverDiv.append($linkIcon);
    $hoverDiv.append($heading);
    $hoverDiv.append($subheading);
    return $hoverDiv

}

/**
 * SLIDER
 */

$(".sl-one").slick({
    // autoplay: true,  TODO: Почему-то очень лагает при автоплее?!
    // autoplaySpeed: 2000,
    // speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    pauseOnFocus: false,
    asNavFor: ".sl-two",
    infinite: true,
});

$(".sl-two").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    centerMode: true,
    variableWidth: true,
    focusOnSelect: true,
    arrows: true,
    asNavFor: ".sl-one"
});

/**
 * Gallery of best images MASONRY
 */

(function () {
    const $loader = $('.gallery-of-best-images-section .loader').hide();
    const $loadBtn = $('.gallery-of-best-images-section .btn');
    const $container = $('.best-images');

    $container.masonry({
        percentPosition: true,
        columnWidth: '.grid-sizer',
        itemSelector: '.grid-item',
        gutter: 4
    });

    const images = [
        [
            'random-images',
            [
                '1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg', '8.jpg', '9.jpg'
            ]
        ],
        [
            'web-design',
            [
                'web-design1.jpg', 'web-design2.jpg', 'web-design3.jpg',
                'web-design4.jpg', 'web-design5.jpg', 'web-design6.jpg',
                'web-design7.jpg'
            ]
        ],
        [
            'graphic-design',
            [
                'graphic-design1.jpg', 'graphic-design2.jpg', 'graphic-design3.jpg',
                'graphic-design4.jpg', 'graphic-design5.jpg', 'graphic-design6.jpg',
                'graphic-design7.jpg', 'graphic-design8.jpg', 'graphic-design9.jpg',
                'graphic-design10.jpg', 'graphic-design11.jpg', 'graphic-design12.jpg'
            ]
        ],
        [
            'landing-page',
            [
                'landing-page1.jpg', 'landing-page2.jpg', 'landing-page3.jpg',
                'landing-page4.jpg', 'landing-page5.jpg', 'landing-page6.jpg',
                'landing-page7.jpg'
            ]
        ],
        [
            'wordpress',
            [
                'wordpress1.jpg', 'wordpress2.jpg', 'wordpress3.jpg',
                'wordpress4.jpg', 'wordpress5.jpg', 'wordpress6.jpg',
                'wordpress7.jpg', 'wordpress8.jpg', 'wordpress9.jpg',
                'wordpress10.jpg'
            ]
        ],
        [
            'gallery-of-best-images',
            [
                '1.png', '2.png', '3.png',
                '4.png', '5.png', '6.png',
                '7.png', '8.png', '9.png',
                '10.png', '11.png', '12.png',
                '13.png', '14.png', '15.png'
            ]
        ],
        [
            'our-services',
            [
                'web-design.png', 'graphic-design.png', 'online-support.png',
                'app-design.png', 'online-marketing.png', 'seo-service.png'
            ]
        ]
    ];

    function getImageSrc(array) {
        const folderIndex = randomIndex(array.length);
        const folder = array[folderIndex];
        const imageIndex = folder[1].splice(randomIndex(folder[1].length), 1);

        if (folder[1].length === 0) {
            array.splice(folderIndex, 1);
        }
        return `images/${folder[0]}/${imageIndex}`;
    }

    function randomIndex(length) {
        return Math.ceil(Math.random() * length) - 1;
    }

    function createHoverDiv() {
        const $hoverDiv = $('<div class="best-hover-div">');
        const $searchIcon = $('<i class="fas fa-search">');
        const $expandIcon = $('<i class="fas fa-expand">');
        $hoverDiv.append($searchIcon);
        $hoverDiv.append($expandIcon);
        return $hoverDiv;
    }

    function createGalleryItem() {
        const hoverDiv = createHoverDiv();
        const imageSrc = getImageSrc(images);
        const $newGalleryItem = $('<div hidden>');

        const $newImage = $(`<img class='grid-item-img' src='${imageSrc}' alt="image">`); //TODO: Проверить img class!
        $newImage.on('load', function () {
            const width = $newImage.prop('width');
            if (width < 700) {
                $newGalleryItem.addClass('medium-size');
            } else if (width > 700) {
                $newGalleryItem.addClass('big-size');
            }
            $newGalleryItem.addClass('grid-item');
        });

        $newGalleryItem.append(hoverDiv).append($newImage);
        $container.append($newGalleryItem);
        $container.imagesLoaded(function () {
            $container.masonry('addItems', $newGalleryItem);
        });
    }

    function buttonClicked() {
        $loadBtn.hide(1);
        $loader.show(1);
        setTimeout(loadImages, 2000);
    }

    function loadImages() {
        for (let i = 0; i < 24; i++) {
            createGalleryItem();
            if (!images.length) {
                $($loadBtn).remove();
                break;
            }
        }

        $container.imagesLoaded(function () {
            $('.grid-item').removeAttr('hidden');
            $container.masonry('layout');
        });
        setTimeout(() => {
            $($loadBtn).show(1);
            $loader.hide(1)
        }, 0)
    }

    loadImages();
    $loadBtn.on('click', buttonClicked);

})();

/**
 * BTN ↑
 */

const $btnTop = $('.btn-to-top');
const $windowHeight = $(window).height();

$btnTop.on('click', () => {
    $('html').animate({scrollTop: 0}, 700);
});

$('body').on('click', '.menu-item a', event => {
    const targetId = $(event.target).attr('href');
    const anchorOffset = $(`${targetId}`).offset();
    $('html').animate({scrollTop: anchorOffset.top}, 700);
});


$(window).on('scroll', () => {
    if ($(window).scrollTop() >= $windowHeight) {
        $btnTop.fadeIn();
    } else {
        $btnTop.fadeOut();
    }
});


/**
 * activeMenu
 */

$('a[href^="#"]').click(function (event) {
    event.preventDefault();
    const elementClick = $(event.target).attr('href');
    const destination = $(elementClick).offset().top;

    $('html').animate({scrollTop: destination}, 1500);

    return false;

});








