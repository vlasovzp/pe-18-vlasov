const btnEl = document.querySelector('.change-style');
btnEl.addEventListener('click', setStyle);

const trEl = [...document.querySelectorAll('tr')];

if (!localStorage.getItem('theme')) {
    localStorage.setItem('theme', 'false');
}
if (localStorage.theme === 'true') {
    changeTheme();
}

function setStyle() {
    if (localStorage.theme === "false") {
        localStorage.theme = "true";
    } else {
        localStorage.theme = "false";
    }
    changeTheme();
}

function changeTheme() {
    for (let elem of trEl) {
        elem.classList.toggle('dark-theme');
    }
}