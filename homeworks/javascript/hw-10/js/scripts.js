const form = document.querySelector('.password-form');
form.addEventListener("input", removeError);

const showPassword = form.querySelectorAll('.show-password');
showPassword.forEach(item => {
    item.addEventListener('click', toggleType)
});

const inputsEl = form.querySelectorAll('input');
const btnEl = form.querySelector('button');
btnEl.addEventListener("click", clickSubmit);

function toggleType(event) {
    const inputEl = this.closest('.input-wrapper').querySelector('.password');
    if (inputEl.type === 'password') {
        inputEl.type = 'text';
        event.target.classList.toggle('fa-eye');
        event.target.classList.toggle('fa-eye-slash');
    } else {
        inputEl.type = 'password';
        event.target.classList.toggle('fa-eye-slash');
        event.target.classList.toggle('fa-eye');
    }
}

function verificationPassword() {
    return inputsEl[0].value === inputsEl[1].value;
}

function clickSubmit(event) {
    event.preventDefault();
    const passwordPassed = verificationPassword();
    if (passwordPassed) {
        alert('You are welcome.');
    } else {
        showError();
    }
    return false
}

function showError() {
    const divElErr = document.createElement('div');
    divElErr.innerText = "Нужно ввести одинаковые значения";
    divElErr.classList.add("error-message");
    inputsEl[1].after(divElErr);

}

function removeError(event) {
    const target = event.target;
    if (target.tagName !== "INPUT") return;
    const errorDiv = form.querySelector(".error-message");
    if (!errorDiv) return;
    errorDiv.remove();
}

