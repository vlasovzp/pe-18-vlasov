import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

class Button extends Component {
    render() {
        const { style, text, backgroundColor, onClick, className } = this.props;

        return (
            <button
                className={ `btn ${className || ''}` }
                style={{backgroundColor, ...style}}
                onClick={onClick}
            >
                {text}
            </button>
        );
    }
}

Button.propTypes = {
    style: PropTypes.object,
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    className: PropTypes.string,
};

Button.defaultProps = {
    text: 'Some text!',
}

export default Button;