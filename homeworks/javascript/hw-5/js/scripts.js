const createNewUser = () => {
    let firstName = prompt('Enter your first name.');
    let lastName = prompt('Enter your last name.');
    let birthday = prompt('Enter your date of birth.', 'dd.mm.yyyy');

    return {
        firstName: firstName,
        lastName: lastName,
        age: birthday,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function () {
            let dateArr = this.age.split('.');
            let formattedString = dateArr.reverse().join('-');
            let ageDiffMs = Date.now() - new Date(formattedString);
            let ageDiff = new Date(ageDiffMs).getFullYear();
            let userAge = ageDiff - 1970;
            return userAge;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.age.slice(6);
        },
    };
};
const user = createNewUser();

console.log(user.getAge());
console.log(user.getPassword());
