const TAB = function () {
    const tabTitle = document.querySelectorAll('.tabs-title');
    const tabContent = document.querySelectorAll('.tab');
    let tabName = null;

    tabTitle.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });

    function selectTabNav() {
        tabTitle.forEach(item => {
            item.classList.remove('active')
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }

    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
        })
    }
};

TAB();