import React from 'react';
import PropTypes from 'prop-types';

function Star(props) {
    const {style, fill, onClick, color, width, height} = props;

    return (
        <svg
            width={width}
            height={height}
            stroke={color}
            viewBox="0 0 24 24"
            style={style}
            fill={fill ? color : "none"}
            onClick={onClick}>
            <path
                d="M12 5.173l2.335 4.817 5.305.732-3.861 3.71.942 5.27-4.721-2.524-4.721 2.525.942-5.27-3.861-3.71 5.305-.733 2.335-4.817z"/>
        </svg>
    );
}

Star.propTypes = {
    style: PropTypes.object,
    fill: PropTypes.bool,
    onClick: PropTypes.func,
    color: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
};

Star.defaultProps = {
    width: "24",
    height:"24",
    fill: false,
    color: "gold",
}

export default Star;



