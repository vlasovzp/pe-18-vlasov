const userYearOfBirth = Number(prompt('Введите год рождения пользователя!'));
const userMonthOfBirth = Number(prompt('Введите месяц рождения Пользователя!'));
const userDayOfBirth = Number(prompt('Введите день рождения пользователя!'));

const nowYear = 2019;
const nowMonth = 12;
const nowDay = 23;


if (isNaN(userYearOfBirth) || userYearOfBirth === 0) {
    alert('Смотри, что вводишь!');
    throw new Error('Оибка ввода.');
}

if (isNaN(userMonthOfBirth) || userMonthOfBirth === 0) {
    alert('Смотри, что вводишь!');
    throw new Error('Оибка ввода.');
}

if (isNaN(userDayOfBirth) || userDayOfBirth === 0) {
    alert('Смотри, что вводишь!');
    throw new Error('Оибка ввода.');
}

if (userYearOfBirth > nowYear) {
    alert('Год рождения Пользователя должен быть меньше текущего!');
    throw new Error('Оибка ввода.');
}
/**
 * а это пример
 * многострочного
 * коментария
 */
// !!!! Здесь начинается ВЫЧИСЛЕНИЯ --- !!!!
let resultYear; //для хранения результата по году
let resultMonth; // ...
let resultDay; // ...

if (nowMonth >= userMonthOfBirth && nowDay >= userDayOfBirth) {
    resultYear = nowYear - userYearOfBirth;
    resultMonth = nowMonth - userMonthOfBirth;
    resultDay = nowDay - userDayOfBirth;
}
else if (nowDay >= userDayOfBirth && nowMonth < userMonthOfBirth) {
    resultYear = nowYear - userYearOfBirth - 1;
    resultMonth = nowMonth + 12 - userMonthOfBirth;
    resultDay = nowDay - userDayOfBirth;
}

console.log(resultYear, resultMonth, resultDay);


