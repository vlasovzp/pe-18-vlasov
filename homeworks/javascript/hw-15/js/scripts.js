const getNumber = function () {
    let number;
    while (isNaN(number) || number === '' || number % 1 > 0) {
        number = prompt('Please enter your number')
    }
    return number;
};

const factorial = function (num) {

    if (num) {
        return num * factorial(num - 1);
    } else {
        return 1;
    }
};

document.write(factorial(getNumber()));