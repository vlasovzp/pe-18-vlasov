const CONTAINER_EL = document.createElement('div');
CONTAINER_EL.classList.add('container');
document.body.prepend(CONTAINER_EL);

const INPUT_EL = document.createElement('input');
INPUT_EL.classList.add('input');
INPUT_EL.setAttribute('type', 'number');
CONTAINER_EL.append(INPUT_EL);

const TITLE_EL = document.createElement('label');
TITLE_EL.innerText = 'Price';
INPUT_EL.before(TITLE_EL);

const PAR_EL = document.createElement('p');
PAR_EL.innerText = 'Please enter correct price';
INPUT_EL.after(PAR_EL);
PAR_EL.style.visibility = 'hidden';

const SPAN_EL = document.createElement('span');
CONTAINER_EL.prepend(SPAN_EL);
SPAN_EL.style.visibility = 'hidden';

const BUTTON_EL = document.createElement('button');
BUTTON_EL.classList.add('btn-x');
BUTTON_EL.innerText = 'X';
SPAN_EL.after(BUTTON_EL);
BUTTON_EL.style.visibility = 'hidden';

const ON_INPUT_FOCUSED = () => INPUT_EL.classList.add('focused');

const ON_INPUT_BLURRED = () => {

    if (INPUT_EL.value !== '') {
        if (INPUT_EL.value < 0) {
            INPUT_EL.classList.add('err');
            PAR_EL.style.visibility = 'visible';
            BUTTON_EL.style.visibility = 'hidden';
            SPAN_EL.style.visibility = 'hidden';
            return;
        }
        PAR_EL.style.visibility = 'hidden';
        INPUT_EL.classList.remove('err')
        SPAN_EL.style.visibility = 'visible';
        BUTTON_EL.style.visibility = 'visible';
        SPAN_EL.innerText = `Текущая цена: ${INPUT_EL.value}`;
        BUTTON_EL.addEventListener('click', (event) => {
            INPUT_EL.value = '';
            SPAN_EL.style.visibility = 'hidden';
            BUTTON_EL.style.visibility = 'hidden';
            INPUT_EL.classList.remove('focused');
        })


    }
}

INPUT_EL.addEventListener('focus', ON_INPUT_FOCUSED);
INPUT_EL.addEventListener('blur', ON_INPUT_BLURRED);



