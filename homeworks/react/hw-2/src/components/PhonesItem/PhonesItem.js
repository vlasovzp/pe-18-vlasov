import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styles from './PhonesItem.module.scss';
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import Star from "../Icons/Star/Star";


class PhonesItem extends Component {
    state = {
        modal: {
            isActive: false,
            content: null,
        },
    };

    addToCart = () => {
        const {id} = this.props;
        const added = JSON.parse(localStorage.getItem('addedToCard')) || {};
        added[id] = added[id] ? added[id] + 1 : 1;
        localStorage.setItem('addedToCard', JSON.stringify(added));

        this.setState({
            modal: {
                isActive: false,
            },
        });
    };

    addToCartClickHandler = () => {
        const {name} = this.props;
        const content = (
            <Modal
                header='Adding to card'
                closeButton
                text={`Do you want to add ${name} to your cart?`}
                closeModal={this.closeModal}
                actions={[
                    <Button key='1' text='Yes' backgroundColor="#b3382c" onClick={this.addToCart}/>,
                    <Button key='2' text='No' backgroundColor="#b3382c" onClick={this.closeModal}/>,
                ]}
            />
        );

        this.setState({
            modal: {
                isActive: true,
                content,
            },
        });
    };

    closeModal = e => {
        if (e.target === e.currentTarget) {
            this.setState({
                modal: {
                    isActive: false,
                },
            });
        }
    };

    render() {
        const {imageURL, name, price, code, color, id, isFavorite, toggleFavorite} = this.props;
        const {modal} = this.state;

        return (
            <div className={styles.item}>
                <div className={styles.img_wrapper}>
                    <Star
                        style={{
                            position: 'absolute',
                            right: '20px',
                            top: '20px',
                            cursor: 'pointer',
                        }}
                        onClick={() => toggleFavorite(id)}
                        color='orangered'
                        fill={isFavorite}
                        width='30'
                        height='30'
                    />
                    <img src={imageURL} alt={name}/>
                </div>
                <p className={styles.name}>{name}</p>
                <p>Code: {code}</p>
                <p>Color: {color}</p>
                <div className={styles.bottom}>
                    <p className={styles.price}>{price} UAH</p>
                    <Button backgroundColor='yellowgreen' text='Add to cart' onClick={this.addToCartClickHandler}/>
                </div>
                {modal.isActive && modal.content}
            </div>
        );
    }
}

PhonesItem.propTypes = {
    imageURL: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    code: PropTypes.number,
    color: PropTypes.string,
    id: PropTypes.number,
    isFavorite: PropTypes.bool,
    toggleFavorite: PropTypes.func,
};

PhonesItem.defaultProps = {
    isFavorite: false,
};

export default PhonesItem;