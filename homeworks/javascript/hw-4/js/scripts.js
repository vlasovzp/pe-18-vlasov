  let firstName;
  let lastName;
  /**
   * @desc Create new user / user login
   * @returns {{getLogin: (function(): string), firstName: string, lastName: string}}
   */
  const createNewUser = function () {
     firstName = prompt('Enter your first name.');
     lastName = prompt('Enter your last name.');

    return {
        firstName: firstName,
        lastName: lastName,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    }
  };

  const newUser = createNewUser();
  console.log(newUser.getLogin());
