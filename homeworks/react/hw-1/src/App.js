import React, {Component} from 'react';
import "./App.css"
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {

    state = {
        modal: {
            isActive: false,
            content: null,
        },
    };

    openFirstModal = () => {
        const content = (
            <Modal
                closeModal={this.closeModal}
                header='Do you want to delete this file'
                closeButton
                text='Once you delete this file, it won’t be possible to undo this action.
			Are you sure you want to delete it?'
                actions={[
                    <Button key='1' className='btn' text='Ok' backgroundColor='#b3382c'/>,
                    <Button key='2' className='btn' text='Cancel' backgroundColor='#b3382c'/>,
                ]}
            />
        );

        this.setState({
            modal:{
                isActive: true,
                content,
            },
        });
    };

    openSecondModal = () => {
        const content = (
            <Modal
                closeModal={this.closeModal}
                header='Do you want to save this file'
                closeButton
                text='Are you sure you want to save it?'
                actions={[
                    <Button key='1' className='btn' text='Yes' backgroundColor='#b3382c'/>,
                    <Button key='2' className='btn' text='No' backgroundColor='#b3382c'/>,
                ]}
            />
        );

        this.setState({
            modal:{
                isActive: true,
                content,
            },
        });
    };

    closeModal = e => {
        if (e.target === e.currentTarget) {
            this.setState({
                modal: {
                    isActive: false,
                },
            });
        }
    };

    render() {
        const { modal } = this.state

        return (
            <div className='App'>
                <div>
                    <Button className='btn' text='Open first modal' backgroundColor='darkRed' onClick={this.openFirstModal}/>
                    <Button className='btn' text='Open second modal' backgroundColor='#b3382c' onClick={this.openSecondModal}/>
                </div>
                {modal.isActive && modal.content}
            </div>
        );
    }
}

export default App;