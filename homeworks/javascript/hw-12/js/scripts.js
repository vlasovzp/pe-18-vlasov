const imgWrapper = document.querySelector('.images-wrapper');

const btnWrapper = document.createElement('div');
btnWrapper.classList.add('btn-wrapper');
imgWrapper.after(btnWrapper);

const images = [...document.querySelectorAll('.image-to-show')];

const playBtn = document.createElement('button');
playBtn.classList.add('play-btn');
playBtn.innerText = 'Возобновить показ';
playBtn.addEventListener('click', playBtnClick);

const pauseBtn = document.createElement('button');
pauseBtn.classList.add('pause-btn');
pauseBtn.innerText = 'Прекратить';
pauseBtn.addEventListener('click', pauseBtnClick);

btnWrapper.append(playBtn);
btnWrapper.append(pauseBtn);

let interval = null;
let counter = 1;
let activeImg = null;

setTimeout(function () {
    images[0].classList.remove('image-active');
}, 10000);

function playBtnClick() {
    interval = setInterval(function () {
        if(activeImg) {
            activeImg.classList.remove('image-active');
        }
        if (counter < images.length) {
            activeImg = images[counter];
            activeImg.classList.add('image-active');
            counter++
        }
        if (counter === images.length) {
            counter = 0;
        }
    }, 10000);
}

function pauseBtnClick() {
    clearInterval(interval);
}

playBtnClick();