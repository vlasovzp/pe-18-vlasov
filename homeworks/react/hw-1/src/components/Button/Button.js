import React, {Component} from 'react';
import './Button.scss';

class Button extends Component {
    render() {
        const { style } = this.props;
        const { text, backgroundColor, onClick, className} = this.props;

        return (
            <button
                className={className}
                style={{backgroundColor, ...style}}
                onClick={onClick}
            >
                {text}
            </button>
        );
    }
}

export default Button;