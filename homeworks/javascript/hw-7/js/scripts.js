const arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

const showList = (arr) => {
    const list = document.createElement('ul');
    const liArray = arr.map(item => `<li>${item}</li>`);
    list.innerHTML = liArray.join('');
    document.body.append(list)
};
showList(arr);

const timer = () => {
    const time = document.createElement('p');
    let counter = 10;
    time.innerText = counter;
    let interval = setInterval(() => {
        counter --;
        time.innerText = counter;
        if(counter === 0) {
            document.body.innerHTML = '';
            clearInterval(interval)
        }
    }, 1000);
    document.body.append(time);
};
timer();