import React, {Component} from 'react';
import "./App.css";
import axios from 'axios';
import PhonesList from "./components/PhonesList/PhonesList";

class App extends Component {

    state = {
        isLoading: true,
        phones: [],
    };

    componentDidMount = async () => {
        const req = await axios('phones.json');
        this.setState({ phones: req.data, isLoading: false });
    };

    render() {
        const { phones, isLoading} = this.state;

        return (
            <div className='App'>{ isLoading ? <p>Loading...</p> : <PhonesList phones = {phones}/> }</div>
        );
    }
}

export default App;