import React, {Component} from 'react';
import PropTypes from 'prop-types';
import PhonesItem from "../PhonesItem/PhonesItem";
import './PhonesList.scss'

class PhonesList extends Component {
    state = {
        favorites: JSON.parse(localStorage.getItem('favorites')) || [],
    };

    toggleFavorite = id => {
        const {favorites} = this.state;

        if (favorites.includes(id)) {
            const newFavorites = favorites.filter(item => item !== id);
            this.setState({favorites: newFavorites});
            localStorage.setItem('favorites', JSON.stringify(newFavorites));
        } else {
            const newFavorites = [...favorites, id];
            this.setState({favorites: newFavorites});
            localStorage.setItem('favorites', JSON.stringify(newFavorites));
        }
    };

    render() {
        const {favorites} = this.state;
        const {phones} = this.props;

        const list = phones.map((itemData, index) => {
            const isFavorite = favorites.includes(itemData.id);
            return <PhonesItem {...itemData} key={index} isFavorite={isFavorite} toggleFavorite={this.toggleFavorite}/>
        });

        return <div className='phones-list'>{list}</div>
    }
}

PhonesList.propTypes = {
    phones: PropTypes.array,
};

export default PhonesList;