let userNumber;

do {
    userNumber = Number(prompt("Please, enter number"))
} while (isNaN(userNumber) || userNumber % 1 > 0);

if (userNumber < 5 && userNumber >= 0) {
    console.log("Sorry, no numbers");
} else if (userNumber > 0) {
    for (let n = 0; n <= userNumber; n +=5) {
        if (n % 5 === 0) {
            console.log(n);
        }
    }
} else {
    alert(`Sorry, only positive number.`);
}
