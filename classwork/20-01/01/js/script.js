{
    const SIZE = 3;
    let result = 0;

    for (let i = 0; i < SIZE; i++) {
        for (let j = 0; j < SIZE; j++) {
            if (i === j) {
                result += Number(
                    (Math.random() * 100).toFixed()
                );
            }
        }
    }

    console.log(result);

}